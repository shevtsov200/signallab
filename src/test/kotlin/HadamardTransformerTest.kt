import junit.framework.Assert.assertTrue
import org.junit.Test

class HadamardTransformerTest {

    companion object {
        private val HADAMARD_FOUR_ORIGINAL = arrayOf(0.0, 0.0, 4.0, 4.0, 5.0, 6.0, 0.0, 4.0)
        private val HADAMARD_FOUR_EXPECTED = arrayOf(23.0, -7.0, -15.0, -1.0, 3.0, -3.0, 5.0, -5.0)

        private val HADAMARD_TRANSFORM_ORIGINAL = arrayOf(
            arrayOf(0, 0, 4, 4, 5, 6, 0, 4),
            arrayOf(2, 3, 2, 3, 4, 0, 2, 2),
            arrayOf(0, 4, 4, 5, 3, 4, 3, 5),
            arrayOf(2, 3, 2, 4, 5, 6, 4, 0),
            arrayOf(0, 2, 2, 5, 0, 0, 2, 2),
            arrayOf(0, 3, 4, 3, 4, 3, 4, 3),
            arrayOf(2, 3, 4, 0, 0, 1, 2, 2),
            arrayOf(0, 0, 0, 3, 3, 2, 0, 0)
        ).toDoubleMatrix()

        private val HADAMARD_TRANSFORM_EXPECTED = arrayOf(
            arrayOf(154.0, -8.0, -36.0, -14.0, -4.0, -10.0, -18.0, -16.0),
            arrayOf(36.0, -14.0, -18.0, 12.0, 6.0, 0.0, 0.0, -6.0),
            arrayOf(-28.0, 2.0, -2.0, 12.0, 14.0, -8.0, 16.0, 10.0),
            arrayOf(2.0, 0.0, -4.0, -14.0, 12.0, -10.0, -2.0, 0.0),
            arrayOf(-14.0, -8.0, -28.0, 10.0, 12.0, 22.0, -2.0, -8.0),
            arrayOf(20.0, -14.0, -10.0, 12.0, -18.0, 0.0, 16.0, 10.0),
            arrayOf(12.0, -22.0, -26.0, -4.0, 6.0, -8.0, 8.0, -14.0),
            arrayOf(2.0, 8.0, 4.0, -22.0, -4.0, -10.0, 22.0, -16.0)
        )

        private val HADAMARD_NORMALIZE_ORIGINAL = arrayOf(
            arrayOf(154.0, -8.0, -36.0, -14.0, -4.0, -10.0, -18.0, -16.0),
            arrayOf(36.0, -14.0, -18.0, 12.0, 6.0, 0.0, 0.0, -6.0),
            arrayOf(-28.0, 2.0, -2.0, 12.0, 14.0, -8.0, 16.0, 10.0),
            arrayOf(2.0, 0.0, -4.0, -14.0, 12.0, -10.0, -2.0, 0.0),
            arrayOf(-14.0, -8.0, -28.0, 10.0, 12.0, 22.0, -2.0, -8.0),
            arrayOf(20.0, -14.0, -10.0, 12.0, -18.0, 0.0, 16.0, 10.0),
            arrayOf(12.0, -22.0, -26.0, -4.0, 6.0, -8.0, 8.0, -14.0),
            arrayOf(2.0, 8.0, 4.0, -22.0, -4.0, -10.0, 22.0, -16.0)
        )

        private val HADAMARD_NORMALIZE_EXPECTED = arrayOf(
            arrayOf(9856.0, -512.0, -2304.0, -896.0, -256.0, -640.0, -1152.0, -1024.0),
            arrayOf(2304.0, -896.0, -1152.0, 768.0, 384.0, 0.0, 0.0, -384.0),
            arrayOf(-1792.0, 128.0, -128.0, 768.0, 896.0, -512.0, 1024.0, 640.0),
            arrayOf(128.0, 0.0, -256.0, -896.0, 768.0, -640.0, -128.0, 0.0),
            arrayOf(-896.0, -512.0, -1792.0, 640.0, 768.0, 1408.0, -128.0, -512.0),
            arrayOf(1280.0, -896.0, -640.0, 768.0, -1152.0, 0.0, 1024.0, 640.0),
            arrayOf(768.0, -1408.0, -1664.0, -256.0, 384.0, -512.0, 512.0, -896.0),
            arrayOf(128.0, 512.0, 256.0, -1408.0, -256.0, -640.0, 1408.0, -1024.0)
        )

        private val HADAMARD_REVERSE_TRANSFORM_ORIGINAL = arrayOf(
            arrayOf(98990.5456, -18094.5456, -32430.5456, 12974.5456, -22190.5456, 8878.5456, 4782.5456, -28334.5456),
            arrayOf(64756.364, -47348.364, -49396.364, 52468.364, -43252.364, 40180.364, 40180.364, -49396.364),
            arrayOf(31988.364, -39156.364, -41204.364, 52468.364, -39156.364, 36084.364, 48372.364, -41204.364),
            arrayOf(21166.5456, -13998.5456, -16046.5456, 12974.5456, -13998.5456, 8878.5456, 12974.5456, -20142.5456),
            arrayOf(-53492.364, 36084.364, 25844.364, -41204.364, 52468.364, -28916.364, -41204.364, 42228.364),
            arrayOf(-9902.5456, 6830.5456, 8878.5456, -13998.5456, 10926.5456, -13998.5456, -5806.5456, 25262.5456),
            arrayOf(-13998.5456, 2734.5456, 686.5456, -22190.5456, 23214.5456, -18094.5456, -9902.5456, 12974.5456),
            arrayOf(-45300.364, 44276.364, 42228.364, -57588.364, 44276.364, -45300.364, -28916.364, 38132.364)
        )

        private val HADAMARD_REVERSE_TRANSFORM_EXPECTED = arrayOf(
            arrayOf(
                -5.6843418860808015E-14,
                5.6843418860808015E-14,
                2047.9999999999998,
                2048.0,
                2560.0,
                3072.0,
                5.6843418860808015E-14,
                2048.0
            ),
            arrayOf(1024.0, 4608.0, 1023.9999999999997, 1536.0, 2048.0, 30161.4548, 1024.0, 1024.0),
            arrayOf(-8.526512829121202E-14, 2048.0, 2047.9999999999995, 2560.0, 1536.0, 2048.0, 1536.0, 2560.0),
            arrayOf(
                1024.0,
                1535.9999999999998,
                1023.9999999999999,
                2048.0,
                2560.0,
                3072.0,
                2048.0,
                -1.1368683772161603E-13
            ),
            arrayOf(
                -5.6843418860808015E-14,
                1024.0000000000002,
                1023.9999999999997,
                2560.0,
                0.0,
                -13090.909200000002,
                1024.0,
                1023.9999999999999
            ),
            arrayOf(-2.8421709430404007E-14, 1536.0, 2048.0, 1536.0, 2048.0, 1536.0, 2048.0, 1536.0),
            arrayOf(1024.0, 1536.0, 2048.0, 0.0, 0.0, 512.0, 1024.0, 1023.9999999999998),
            arrayOf(
                -5.6843418860808015E-14,
                -5.6843418860808015E-14,
                -2.2737367544323206E-13,
                1536.0,
                1536.0,
                1023.9999999999991,
                -5.6843418860808015E-14,
                0.0
            )
        )

        private fun Array<Array<Int>>.toDoubleMatrix() = map { line ->
            line.map(Int::toDouble).toTypedArray()
        }.toTypedArray()
    }

    private val hadamardTransformer = HadamardTransformer()

    @Test
    fun testHadamardFour() {
        val transformedLine = hadamardTransformer.hadamardFour(HADAMARD_FOUR_ORIGINAL)

        assertTrue(HADAMARD_FOUR_EXPECTED contentEquals transformedLine)
    }

    @Test
    fun testHadamardTransform() {
        val transformedMatrix = hadamardTransformer.transform(HADAMARD_TRANSFORM_ORIGINAL)

        assertTrue(HADAMARD_TRANSFORM_EXPECTED contentDeepEquals transformedMatrix)
    }

    @Test
    fun testHadamardNormalization() {
        val normalizedMatrix = hadamardTransformer.normalize(HADAMARD_NORMALIZE_ORIGINAL)

        assertTrue(HADAMARD_NORMALIZE_EXPECTED contentDeepEquals normalizedMatrix)
    }

    @Test
    fun testHadamardReverseTransform() {
        val transformedMatrix = hadamardTransformer.reverseTransform(HADAMARD_REVERSE_TRANSFORM_ORIGINAL)

        assertTrue(HADAMARD_REVERSE_TRANSFORM_EXPECTED contentDeepEquals transformedMatrix)
    }

    @Test
    fun testMultiply() {
        val a = arrayOf(arrayOf(1, 2), arrayOf(3, 4)).toDoubleMatrix()
        val b = arrayOf(arrayOf(2, 3), arrayOf(4, 5)).toDoubleMatrix()
        val expected = arrayOf(arrayOf(10, 13), arrayOf(22, 29)).toDoubleMatrix()

        val multiplied = hadamardTransformer.multiply(a, b)

        assertTrue(expected contentDeepEquals multiplied)
    }
}
