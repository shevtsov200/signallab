package usecase

import HadamardTransformer
import Matrix
import model.MatricesForPlotting
import model.SignalMatrices
import print
import toDoubleMatrix

class CalculateUseCase(private val hadamardTransformer: HadamardTransformer) {

    companion object {

        private val originalMatrix = arrayOf(
            arrayOf(5, 6, 0, 3, 0, 5, 0, 5),
            arrayOf(5, 4, 0, 4, 0, 5, 0, 3),
            arrayOf(4, 5, 3, 2, 5, 6, 7, 7),
            arrayOf(0, 2, 0, 4, 4, 5, 7, 8),
            arrayOf(5, 0, 6, 5, 7, 8, 7, 9),
            arrayOf(3, 3, 4, 4, 4, 5, 7, 6),
            arrayOf(0, 3, 2, 3, 0, 5, 0, 6),
            arrayOf(4, 4, 5, 7, 5, 9, 8, 4)
        ).toDoubleMatrix()

        private val firstMatrixWithError = arrayOf(
            arrayOf(5, 6, 0, 3, 0, 5, 0, 5),
            arrayOf(5, 4, 0, 4, 0, 5, 0, 3),
            arrayOf(4, 5, 3, 2, 5, 6, 7, 7),
            arrayOf(0, 2, 0, 4, 4, 5, 7, 8),
            arrayOf(5, 0, 6, 5, 7, 12, 7, 9),
            arrayOf(3, 3, 4, 4, 4, 5, 7, 6),
            arrayOf(0, 3, 2, 3, 0, 5, 0, 6),
            arrayOf(4, 4, 5, 7, 5, 9, 8, 4)
        ).toDoubleMatrix()

        private val secondMatrixWithError = arrayOf(
            arrayOf(5, 6, 0, 3, 0, 5, 0, 5),
            arrayOf(5, 4, 12, 4, 0, 5, 0, 3),
            arrayOf(4, 5, 3, 2, 5, 6, 7, 7),
            arrayOf(0, 2, 0, 4, 4, 5, 7, 8),
            arrayOf(5, 0, 6, 5, 7, 8, 7, 9),
            arrayOf(3, 3, 4, 4, 4, 5, 7, 6),
            arrayOf(0, 3, 2, 3, 0, 5, 0, 6),
            arrayOf(4, 4, 5, 7, 5, 9, 8, 4)
        ).toDoubleMatrix()

        private val filterMatrix = arrayOf(
            arrayOf(-5.10184, 6.10184, 6.10184, -6.10184, 6.10184, -6.10184, -6.10184, 6.10184),
            arrayOf(-5.78873, 6.78873, 5.78873, -5.78873, 5.78873, -5.78873, -5.78873, 5.78873),
            arrayOf(16.39, -16.39, -15.39, 16.39, -16.39, 16.39, 16.39, -16.39),
            arrayOf(-8.39978, 8.39978, 8.39978, -7.39978, 8.39978, -8.39978, -8.39978, 8.39978),
            arrayOf(-24.1181, 24.1181, 24.1181, -24.1181, 25.1181, -24.1181, -24.1181, 24.1181),
            arrayOf(20.9707, -20.9707, -20.9707, 20.9707, -20.9707, 21.9707, 20.9707, -20.9707),
            arrayOf(15.0217, -15.0217, -15.0217, 15.0217, -15.0217, 15.0217, 16.0217, -15.0217),
            arrayOf(-1.56013, 1.56013, 1.56013, -1.56013, 1.56013, -1.56013, -1.56013, 2.56013)
        )


    }

    fun calculateMatricesForPlotting(): MatricesForPlotting {
        val originalMatrices = SignalMatrices(
            correctMatrix = originalMatrix,
            matrixWithError1 = firstMatrixWithError,
            matrixWithError2 = secondMatrixWithError
        )

        println("original:")
        originalMatrices.print()

        val hadamardTransformedMatrices = hadamardTransform(originalMatrices)
        println("hadamard transformed:")
        hadamardTransformedMatrices.print()

        val spectrumMatrices = hadamardSpectrum(hadamardTransformedMatrices)
        println("spectrum:")
        spectrumMatrices.print()

        println("filter:")
        filterMatrix.print()

        println("first matrix with error multiplied by filter")
        val firstMatrixWithErrorFiltered = hadamardTransformer.multiply(
            firstMatrixWithError,
            filterMatrix
        )
        firstMatrixWithErrorFiltered.print()

        println("second matrix with error multiplied by filter")
        val secondMatrixWithErrorFiltered = hadamardTransformer.multiply(
            secondMatrixWithError,
            filterMatrix
        )
        secondMatrixWithErrorFiltered.print()

        println("reverse hadamard for filtered first matrix with error")
        val reverseHadamardFirst = hadamardTransformer.reverseTransform(firstMatrixWithErrorFiltered)
        reverseHadamardFirst.print()

        println("reverse hadamard for filtered second matrix with error")
        val reverseHadamardSecond = hadamardTransformer.reverseTransform(secondMatrixWithErrorFiltered)
        reverseHadamardSecond.print()

        return MatricesForPlotting(
            originalMatrices = originalMatrices,
            transformedMatrices = spectrumMatrices,
            filterMatrix = filterMatrix,
            secondMatrixWithErrorFiltered = secondMatrixWithErrorFiltered
        )
    }

    private fun hadamardTransform(signalMatrices: SignalMatrices) =
        signalMatrices.transform { hadamardTransformer.transform(it) }

    private fun hadamardSpectrum(signalMatrices: SignalMatrices) =
        signalMatrices.transform { hadamardTransformer.normalize(it) }

    private fun SignalMatrices.transform(action: (matrix: Matrix) -> Matrix) = copy(
        correctMatrix = action(correctMatrix),
        matrixWithError1 = action(matrixWithError1),
        matrixWithError2 = action(matrixWithError2)
    )

}