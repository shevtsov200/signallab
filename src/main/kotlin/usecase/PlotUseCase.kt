package usecase

import Matrix
import model.MatricesForPlotting
import org.jzy3d.analysis.AbstractAnalysis
import org.jzy3d.analysis.AnalysisLauncher
import org.jzy3d.chart.factories.AWTChartComponentFactory
import org.jzy3d.colors.Color
import org.jzy3d.colors.ColorMapper
import org.jzy3d.colors.colormaps.ColorMapRainbow
import org.jzy3d.maths.Coord3d
import org.jzy3d.plot3d.primitives.Point
import org.jzy3d.plot3d.primitives.Polygon
import org.jzy3d.plot3d.primitives.Shape
import org.jzy3d.plot3d.rendering.canvas.Quality
import java.io.File
import java.util.*


class SurfaceDemo(private val name: String) : AbstractAnalysis() {
    override fun getName(): String = name

    private fun createSurface(matrix: Matrix): Shape {
        val polygons = ArrayList<Polygon>()
        for (i in 0 until matrix.size - 1) {
            for (j in 0 until matrix[i].size - 1) {
                val polygon = Polygon()
                polygon.add(Point(Coord3d(i.toDouble(), j.toDouble(), matrix[i][j])))
                polygon.add(Point(Coord3d(i.toDouble(), (j + 1).toDouble(), matrix[i][j + 1])))
                polygon.add(Point(Coord3d((i + 1).toDouble(), (j + 1).toDouble(), matrix[i + 1][j + 1])))
                polygon.add(Point(Coord3d((i + 1).toDouble(), j.toDouble(), matrix[i + 1][j])))
                polygons.add(polygon)
            }
        }

        val surface = Shape(polygons)
        surface.colorMapper =
            ColorMapper(
                ColorMapRainbow(),
                surface.bounds.zmin.toDouble(),
                surface.bounds.zmax.toDouble(),
                Color(1f, 1f, 1f, .5f)
            )
        surface.faceDisplayed = true
        surface.wireframeDisplayed = true

        return surface
    }

    fun plotSurface(matrix: Matrix) {
        val surface = createSurface(matrix)

        chart.scene.graph.add(surface)
    }

    fun savePlotToFile(matrix: Matrix, fileName: String) {
        plotSurface(matrix)
        val file = File(fileName)
        chart.screenshot(file)

    }

    override fun init() {
        chart = AWTChartComponentFactory.chart(Quality.Advanced, getCanvasType())


    }
}

class PlotUseCase {


    fun plot(matricesForPlotting: MatricesForPlotting) {
        with(matricesForPlotting) {
            plotMatrixToFile(originalMatrices.correctMatrix, "1 original-correct.png")
            plotMatrixToFile(originalMatrices.matrixWithError1, "2 original-error1.png")
            plotMatrixToFile(originalMatrices.matrixWithError2, "3 original-error2.png")

            plotMatrixToFile(transformedMatrices.correctMatrix, "4 transformed-correct.png")
            plotMatrixToFile(transformedMatrices.matrixWithError1, "5 transformed-error1.png")
            plotMatrixToFile(transformedMatrices.matrixWithError2, "6 transformed-error2.png")

            plotMatrixToFile(filterMatrix, "7 filter matrix.png")

            plotMatrixToFile(secondMatrixWithErrorFiltered, "8 filtered-error2.png")
        }

    }

    private fun plotMatrixToFile(matrix: Matrix, fileName: String) {
        val surfaceDemo = SurfaceDemo(fileName)
        AnalysisLauncher.open(surfaceDemo)

        surfaceDemo.savePlotToFile(matrix, fileName)
    }
}