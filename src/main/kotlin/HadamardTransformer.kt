class HadamardTransformer {
    companion object {
        private const val N = 8
    }

    fun transform(originalMatrix: Matrix): Matrix {
        val matrix = originalMatrix.clone()

        for (i in 0 until N) {
            matrix[i] = hadamardFour(matrix[i])
        }


        for (i in 0 until N) {
            val result = hadamardFour(
                arrayOf(
                    matrix[0][i],
                    matrix[1][i],
                    matrix[2][i],
                    matrix[3][i],
                    matrix[4][i],
                    matrix[5][i],
                    matrix[6][i],
                    matrix[7][i]
                )
            )

            for (j in 0 until N) {
                matrix[j][i] = result[j]
            }
        }

        return matrix
    }

    fun normalize(matrix: Matrix): Matrix = matrix.divideBy(1.0 / (N * N))

    fun reverseTransform(originalMatrix: Matrix): Matrix {
        val matrix = originalMatrix.clone()

        for (i in 0 until N) {
            for (j in 0 until N) {
                matrix[i][j] = matrix[i][j] / N
                matrix[i][j] = matrix[i][j] / N
            }
        }

        for (i in 0 until N) {
            matrix[i] = hadamardFourReversed(matrix[i])
        }


        for (i in 0 until N) {
            val result = hadamardFourReversed(
                arrayOf(
                    matrix[0][i],
                    matrix[1][i],
                    matrix[2][i],
                    matrix[3][i],
                    matrix[4][i],
                    matrix[5][i],
                    matrix[6][i],
                    matrix[7][i]
                )
            )

            for (j in 0 until N) {
                matrix[j][i] = result[j]
            }
        }

        return matrix
    }

    fun multiply(firstMatrix: Matrix, secondMatrix: Matrix): Matrix {
        val n = firstMatrix.size
        val product = Array(size = n, init = { DoubleArray(n) })
        for (i in 0 until n) {
            for (j in 0 until n) {
                for (k in 0 until n) {
                    product[i][j] += firstMatrix[i][k] * secondMatrix[k][j]
                }
            }
        }

        return product.map { it.toTypedArray() }.toTypedArray()
    }

    fun hadamardFour(line: Array<Double>): Array<Double> {
        val newline = arrayOf(line[0], line[4], line[2], line[6], line[1], line[5], line[3], line[7])
        val result = Array<Double>(size = N, init = { 0.0 })

        kek(result, newline)

        return result
    }

    private fun hadamardFourReversed(line: Array<Double>): Array<Double> {
        val newline = line.clone()
        val result = Array<Double>(size = N, init = { 0.0 })

        kek(result, newline)

        return arrayOf(result[0], result[7], result[4], result[3], result[2], result[5], result[6], result[1])
    }

    private fun kek(result: Array<Double>, newline: Array<Double>) {
        var newline1 = newline.clone()

        var delta = 4
        for (i in 0..3) {
            result[i] = newline1[i] + newline1[i + delta]
        }
        for (i in 4..7) {
            result[i] = -newline1[i] + newline1[i - delta]
        }

        newline1 = result.clone()
        delta = 2
        for (i in 0..1) {
            result[i] = newline1[i] + newline1[i + delta]
        }
        for (i in 2..3) {
            result[i] = -newline1[i] + newline1[i - delta]
        }

        for (i in 4..5) {
            result[i] = newline1[i] - newline1[i + delta]
        }
        for (i in 6..7) {
            result[i] = newline1[i] + newline1[i - delta]
        }

        newline1 = result.clone()

        result[0] = newline1[0] + newline1[1]
        result[1] = newline1[0] - newline1[1]

        result[2] = newline1[2] - newline1[3]
        result[3] = newline1[2] + newline1[3]

        result[4] = newline1[4] + newline1[5]
        result[5] = newline1[4] - newline1[5]

        result[6] = newline1[6] - newline1[7]
        result[7] = newline1[6] + newline1[7]
    }

    private fun Matrix.divideBy(n: Double) = map { line ->
        line.map { element ->
            element / n
        }.toTypedArray()
    }.toTypedArray()
}