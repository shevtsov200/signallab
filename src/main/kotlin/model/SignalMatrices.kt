package model

import Matrix
import print

data class SignalMatrices(
    val correctMatrix: Matrix,
    val matrixWithError1: Matrix,
    val matrixWithError2: Matrix
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SignalMatrices

        if (!correctMatrix.contentDeepEquals(other.correctMatrix)) return false
        if (!matrixWithError1.contentDeepEquals(other.matrixWithError1)) return false
        if (!matrixWithError2.contentDeepEquals(other.matrixWithError2)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = correctMatrix.contentDeepHashCode()
        result = 31 * result + matrixWithError1.contentDeepHashCode()
        result = 31 * result + matrixWithError2.contentDeepHashCode()
        return result
    }

    fun print() {
        println("correct matrix:")
        correctMatrix.print()

        println("matrix with error 1:")
        matrixWithError1.print()

        println("matrix with error 2:")
        matrixWithError2.print()
    }
}