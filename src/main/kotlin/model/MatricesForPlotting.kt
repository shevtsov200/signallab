package model

import Matrix

data class MatricesForPlotting(
    val originalMatrices: SignalMatrices,
    val transformedMatrices: SignalMatrices,
    val filterMatrix: Matrix,
    val secondMatrixWithErrorFiltered: Matrix
)