import usecase.CalculateUseCase
import usecase.PlotUseCase

fun main(consoleArguments: Array<String>) {
    val calculateUseCase = CalculateUseCase(HadamardTransformer())
    val plotUseCase = PlotUseCase()

    val matricesForPlotting = calculateUseCase.calculateMatricesForPlotting()

    plotUseCase.plot(matricesForPlotting)
}