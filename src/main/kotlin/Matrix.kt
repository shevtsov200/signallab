import java.math.RoundingMode

typealias Matrix = Array<Array<Double>>

fun Array<Array<Int>>.toDoubleMatrix() = map { line ->
    line.map(Int::toDouble).toTypedArray()
}.toTypedArray()

private const val LIST_DEFAULT_DELIMITER_START = "["
private const val LIST_DEFAULT_DELIMITER_END = "]"

private const val MATRIX_START_CHAR = "{"
private const val MATRIX_END_CHAR = "}"

private const val PRINT_NUMBER_DIGITS = 4

fun Matrix.print() {
    println(MATRIX_START_CHAR)
    forEachIndexed { index, line ->
        print(MATRIX_START_CHAR)

        val rowString = line.toList()
            .map {it.toBigDecimal().setScale(PRINT_NUMBER_DIGITS, RoundingMode.UP).toDouble()}
            .toString()
            .removePrefix(LIST_DEFAULT_DELIMITER_START)
            .removeSuffix(LIST_DEFAULT_DELIMITER_END)

        print(rowString)

        if (index == size - 1) {
            println(MATRIX_END_CHAR)
        } else {
            println("$MATRIX_END_CHAR,")
        }
    }
    println(MATRIX_END_CHAR)
}